import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_jms2rdb_containers(host):
    with host.sudo():
        cmd = host.command('docker ps')
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.strip().split('\n')[1:]])
    assert names == ['jms2rdb', 'jms2rdb_postgres']


def test_jms2rdb_index(host):
    cmd = host.command('curl http://localhost:4913/main')
    assert '<title>JMS-to-RDB Tool</title>' in cmd.stdout
