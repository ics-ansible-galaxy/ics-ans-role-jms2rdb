ics-ans-role-jms2rdb
====================

Ansible role to install JMS2RDB.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
jms2rdb_network: jms2rdb-network
jms2rdb_postgres_tag: 10
jms2rdb_postgres_password: password
jms2rdb_postgres_rw_user: log
jms2rdb_postgres_rw_password: secret
jms2rdb_postgres_ro_user: report
jms2rdb_postgres_ro_password: secret
jms2rdb_postgres_db: log
jms2rdb_image: registry.esss.lu.se/ics-docker/jms2rdb
jms2rdb_tag: latest
jms2rdb_jms_url: failover:(tcp://localhost:61616)
jms2rdb_jms_topic: LOG
jms2rdb_jms_filters: "LOG;TEXT=JCACommandThread queue reached"
jms2rdb_postgres_port: "5432"
jms2rdb_http_port: "127.0.0.1:4913"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-jms2rdb
```

License
-------

BSD 2-clause
